const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const port = 4000;
const Schema = mongoose.Schema;

app.use(bodyParser.urlencoded({ extended: false }));

// Connect to MongoDB
mongoose.connect('mongodb://mongo:27017/docker-node-mongo',{ useNewUrlParser: true })
.then(() => console.log('MongoDB Connected'))
.catch(err => console.log(err));

app.get('/', function(req, res) {
  res.json({"hello": "worlds"});
});

const itemSchema = new Schema({
  name:  String,
  id: String,
  hidden: Boolean
});

itemSchema.methods.getName = function(cb) {
  var name = this.name
  console.log(name)
  return name
};

const Item = mongoose.model('Item', itemSchema);

const item = new Item({ name: 'dog' });

abc = item.getName()
console.log(abc)

item.save(function (err, item) {
  if (err) return console.error(err);
});


app.listen(port, () => console.log('listening on port ', port));
