<?php

$title=$_GET['title'];
$values=unserialize(stripslashes($_GET['values']));
//if (!is_array($values)){$values=array();}
sort($values);
$totalvalues=count($values);

$max=$values[$totalvalues-1];
$fontsize=4;

$totalcumfreq=0;
for($i=0;$i<count($values);$i++)
{
$totalcumfreq+=$values[$i];
}

$width=500;
$height=300;
$margin=30;

$im = imageCreate($width+$margin,$height+$margin);
$background = imageColorAllocate($im, 255, 255, 255);
imagecolortransparent($im,$background);
$black = imageColorAllocate ($im, 0, 0, 0); 
$red = imageColorAllocate ($im, 255, 0, 0);
$yellow = imageColorAllocate ($im, 255, 255, 0);
$green = imageColorAllocate ($im, 0, 255, 0);
$blue = imageColorAllocate ($im, 0, 0, 255);

$xbit=$width/$max;
$ybit=$height/$totalvalues;

$oldx=$xbit*$values[0];
$oldy=$height-$ybit*0;

$cumfreq=0;
$oldvalue=1;

for($i=1;$i<count($values);$i++)
{//plots line

//$x=$xbit*$values[$i];
//$y=$height-$ybit*$i;
if($values[$i]!==$oldvalue)
{
$x=$xbit*$values[$i];
$y=$height-$ybit*$cumfreq;
imageline($im, round($oldx)+$margin, round($oldy), round($x)+$margin, round($y), $black);

//imagestring($im, $fontsize, $x-strlen("$values[$i]")*15/2, $height-15 , $values[$i],  $black);

//if($oldx>$height/2 && $x<=$height/2){
//imagestring($im, $fontsize, $x, $height , $cumfreq,  $black);
//}

$oldx=$x;
$oldy=$y;
}
$oldvalue=$values[$i];
$cumfreq++;
}

//plots axis
imageline($im, $margin, 0, $margin, $height, $black);
imageline($im, $margin, $height, $width+$margin, $height, $black);

//plots axis values
//y axis - cumulative frequency
imagestring($im, $fontsize, 0, $height/2 , round($cumfreq/2),  $black);
imagestring($im, $fontsize, 0, $height/4 , round($cumfreq/4*3),  $black);
imagestring($im, $fontsize, 0, $height/4*3 , round($cumfreq/4),  $black);
imagestring($im, $fontsize, 0, 0 , round($cumfreq),  $black);
imagestring($im, $fontsize, 0, $height , 0,  $black);
//x axis - values

imagestring($im, $fontsize, $margin, $height , 0,  $black);
imagestring($im, $fontsize, $margin/2+$width/2, $height , round($max/2),  $black);
imagestring($im, $fontsize, $margin/2+$width/4*3, $height , round($max/4*3),  $black);
imagestring($im, $fontsize, $margin/2+$width/4, $height , round($max/4),  $black);
imagestring($im, $fontsize, $width, $height , round($max),  $black);


header('Content-type: image/png');
imagePNG($im);
imageDestroy($im); 

?>