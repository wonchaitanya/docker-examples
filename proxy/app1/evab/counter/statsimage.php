<?php

require 'settings.php';
require 'functions.php';



if(isset($_GET['min'])){$minvalue=$_GET['min'];}else{$minvalue=1;}
if(isset($_GET['font'])){$fontsize=$_GET['font'];}else{$fontsize=4;}

if(isset($_GET['stats'])){$stats=$_GET['stats'];}else{$stats='';}

if(isset($_GET['charttype'])){$charttype=$_GET['charttype'];}else{$charttype="bar";}
if(isset($_GET['size'])){$chartsize=$_GET['size'];}else{$chartsize=350;}
if($chartsize>600){$chartsize=600;}

if($charttype=='pie'){$minvalue=2;}

function checkdatestuff($test,$extra)
{if(is_numeric($test)){return $test;}
else{switch($extra)
{case 'year';return date("Y");break;
default;return 0;break;}}}

$dbtill=time();
$monthstuff=false;
if(isset($_GET['from'])){$timespan=$_GET['from'];}else{$timespan='';}

switch($timespan)
{case 'hour';
$dbfrom=time()-60*60;
break;
case 'day';
$dbfrom=time()-24*60*60;
break;
case 'month';
//$dbfrom=time()-24*60*60*7*4;
$dbfrom=mktime(date("G"), date("i"), date("s"), date("m")-1, date("d"), date("Y"));
break;
case 'ever';
$dbfrom=0;
break;
case 'date';
$dbfrom=mktime(checkdatestuff($_GET['hourfrom'],false), checkdatestuff($_GET['minutefrom'],false), checkdatestuff($_GET['secondfrom'],false), checkdatestuff($_GET['monthfrom'],false), checkdatestuff($_GET['dayfrom'],false), checkdatestuff($_GET['yearfrom'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
//echo $dbfrom;
//echo '<br>'.date('dS M y H:i',$dbfrom);
//echo "YOOHOO";
break;
case 'specific';//view statistics for a specific month/year/etc.
switch($_GET['specific'])
{case 'month';
default;
$monthstuff=true;
$dbfrom=mktime(0, 0, 0, checkdatestuff($_GET['monthfrom'],false),1, checkdatestuff($_GET['yearfrom'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
$dbtill=mktime(0, 0, 0, checkdatestuff($_GET['monthfrom'],false)+1,1, checkdatestuff($_GET['yearfrom'],'year'), -1);
$specifictitle=date('F Y',$dbfrom);
break;}
break;
case 'week';
default;
$dbfrom=time()-24*60*60*7;
$timespan='week';
break;}

if(isset($_GET['till']))
{
switch($_GET['till'])
{
case 'date';
$dbtill=mktime(checkdatestuff($_GET['hourtill'],false), checkdatestuff($_GET['minutetill'],false), checkdatestuff($_GET['secondtill'],false), checkdatestuff($_GET['monthtill'],false), checkdatestuff($_GET['daytill'],false), checkdatestuff($_GET['yeartill'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
break;
}
}

if(isset($_GET['countby'])){$countby=$_GET['countby'];}
else
{
//if a count by has been set in the url, use it, otherwise use the default from settings.php
$countby=$statdisplaycount;
/*switch($displaycount)
{case 0;$countby=3;break;
case 1;$countby=3;break;
default;$countby=3;break;}*/
}

switch($countby)
{
case 1;
$echocount="Hit";
break;
case 2;
$echocount="Unique Hits";
break;
case 3;
$echocount="IP";
break;
default;
$echocount='Default';
$countby=$howtocount;
break;
}


$browser=array();
$allbrowsers=array();
$browserscount=array();
$browsers=array();

$browser=unserialize(stripslashes($_GET['browser']));
//$browser=unserialize(stripslashes(eregi_replace("\\",'',$_GET['browser'])));
if (!is_array($browser)){$browser=array();}
if(in_array('Other', $browser)){$others=true;}

$others=unserialize(stripslashes($_GET['others']));

if($monthstuff)
{//Day of Hit will be with dates of the month
$daysinmonth=cal_days_in_month(CAL_GREGORIAN, checkdatestuff($_GET['monthfrom'],false), checkdatestuff($_GET['yearfrom'],'year'));
$days=array();
$dayscount=array();
$month=checkdatestuff($_GET['monthfrom'],false);
$year=checkdatestuff($_GET['yearfrom'],'year');
for($i=1;$i<=$daysinmonth;$i++)
{$days[($i-1)]=date('jS', mktime(1,0,0,$month,$i,$year)).' ('.date('D', mktime(1,0,0,$month,$i,$year)).')';
$dayscount[($i-1)]=0;}
}else{
//day of hit will be with day of the week.
$days=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
$dayscount=array();}

$total=0;
$ips=array();
$uniqueips=array();

$pagescount=array();

$versions=array();
$allversions=array();
$versionscount=array();

$OSversions=array();
$OSversions2=array();
$allOSversions=array();
$OSversionscount=array();

$times=array('0000','0100','0200','0300','0400','0500','0600','0700','0800','0900','1000','1100','1200','1300','1400','1500','1600','1700','1800','1900','2000','2100','2200','2300');
$timescount=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

$con = mysql_connect($servername, $dbusername, $dbpassword);
if (!$con){die("Could not connect:" . mysql_error());}
mysql_select_db($dbname, $con);

switch($countby)
{case 2;//unique hits - will require all data
$result = mysql_query("SELECT `time`,`browser`,`version`,`osversion`,`os`,`pages` FROM `$dbtablename` WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."' AND `unique`='1';");
break;
default;
$result = mysql_query("SELECT `ip`,`time`,`browser`,`version`,`osversion`,`os`,`pages` FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."'");
break;}


while($row = mysql_fetch_array($result))
{
$continue=true;

if($countby==2)
{//stats by UNIQUE IPs
/*
if(!in_array($row['ip'], $uniqueips)){$uniqueips[]=$row['ip'];}else{$continue=false;}

if($row['time']<$dbfrom || $row['time']>$dbtill){$continue=false;}
else{

//if it's in the correct time, get data for different IPs
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}
}
*/
$totalhits++;
}else{
//stats for different IPs or hits
$totalhits++;
//checks to see if IP is in the ips array, if not adds it.  if it is then decides whether to continue or not.
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}else{switch($countby){case 3;case 2;$continue=false;break;}}
}

if($_GET['stats']=='version')
{
switch($_GET['browser'])
{
case 'Other';
if(in_array($row['browser'], $others)){$continue=false;}
break;
default;
if($row['browser']!==$_GET['browser']){$continue=false;}
break;
}
}

if($_GET['stats']=='osversion')
{

switch($_GET['os'])
{
case 'Other';
if(in_array($row['os'],$others)){$continue=false;}
break;
default;
if($row['os']!==$_GET['os']){$continue=false;}
break;
}


}

if($continue==true)
{
$total++;

if($_GET['browser']=='Other')//browser name + version number
{
if($charttype=='pie')
{if(!in_array($row['browser'], $versions)){$versions[]=$row['browser'];}
$allversions[]=$row['browser'];}
else
{if(!in_array($row['browser'].' '.$row['version'], $versions)){$versions[]=$row['browser'].' '.$row['version'];}
$allversions[]=$row['browser'].' '.$row['version'];}

}
else//just version number
{

if(!in_array($row['version'], $versions)){$versions[]=$row['version'];}
$allversions[]=$row['version'];
}

if(!in_array($row['browser'], $browsers)){$browsers[]=$row['browser'];}
$allbrowsers[]=$row['browser'];

if(!in_array($row['os'], $OSversions)){$OSversions[]=$row['os'];}
$allOSversions[]=$row['os'];

if(!in_array($row['osversion'], $OSversions2)){$OSversions2[]=$row['osversion'];}
$allOSversions2[]=$row['osversion'];

$timescount[tohours($row['time'])]++;

$pagescount[]=$row['pages'];

if($monthstuff)
{//counts up hits for a specific day of the month.
$dayscount[(date('j',$row['time']))-1]++;
}else{
//counts up hits for day of week
$dayscount[(date('w',$row['time']))]++;}

//end of continue if statement
}
//end of while loop
}

//this works out the quantity of different versions, and then the number of each version.  I was quite impressed I managed to think it though, but I can't remember how it works anymore
for ($i=0;$i<sizeof($versions);$i++)
{
$versionscount[$i]=0;
for ($i2=0;$i2<sizeof($allversions);$i2++)
{if($allversions[$i2]==$versions[$i]){$versionscount[$i]++;}}
}

for ($i=0;$i<sizeof($browsers);$i++)
{
$browserscount[$i]=0;
for ($i2=0;$i2<sizeof($allbrowsers);$i2++)
{if($allbrowsers[$i2]==$browsers[$i]){$browserscount[$i]++;}}
}

for ($i=0;$i<sizeof($OSversions);$i++)
{
$OSversionscount[$i]=0;
for ($i2=0;$i2<sizeof($allOSversions);$i2++)
{if($allOSversions[$i2]==$OSversions[$i]){$OSversionscount[$i]++;}}
}

for ($i=0;$i<sizeof($OSversions2);$i++)
{
$OSversionscount2[$i]=0;
for ($i2=0;$i2<sizeof($allOSversions2);$i2++)
{if($allOSversions2[$i2]==$OSversions2[$i]){$OSversionscount2[$i]++;}}
}



switch($stats)
{
case 'browser';
$options=$browsers;
$values=$browserscount;
$title="Browsers by ".$echocount;
break;
case 'version';
$options=$versions;
$values=$versionscount;

switch($others)
{
case true;
$versionsecho='Other ';
break;
default;
$versionsecho='';
for($i=0;$i<sizeof($browser);$i++){$versionsecho.=$browser[$i].', ';}
break;
}

$title="Versions of ".$_GET['browser']." by ".$echocount;
break;
case 'os';
$options=$OSversions;
$values=$OSversionscount;
$title="Platforms by ".$echocount;
break;
case 'osversion';
switch($_GET['os'])
{
case 'Other';
$options=$OSversions;
$values=$OSversionscount;
$title="Other Platforms by ".$echocount;
break;
default;
$options=$OSversions2;
$values=$OSversionscount2;
$title="Versions of ".$_GET['os']." by ".$echocount;
break;}
break;
case 'time';
$options=$times;
$values=$timescount;
$title="Visit Time (24hrs GMT) by ".$echocount;
break;
case 'day';
$options=$days;
$values=$dayscount;
$title="Day of Visit by ".$echocount;
break;

default;
$options=unserialize($_GET['options']);
$values=unserialize($_GET['values']);
$total=$_GET['total'];
break;
}


if($timespan!=='date' && $timespan!=='specific' && $_GET['till']!=='date')
{
if($timespan!=='ever'){
$title.=" for the last $timespan";
}
else{
$title.=" for all data";
}
}
elseif($_GET['from']=='specific')
{
$title.=" for $specifictitle";
}else{
$title.=" from ".date('d/m/y',$dbfrom)." til ".date('d/m/y',$dbtill);
//echo "<h3>";
}

//echo "Since ".date('H:i\, dS M Y',$dbfrom)." until ".date('H:i\, dS M Y',$dbtill);

//$title.=' for last '.$timespan;



//looks for all the options with less than the minimum percentage of values, and lumps them all into 'other'

$thingsinother=array();

$othervalue=0;
for($i=0;$i<sizeof($values);$i++)
{
if($total!==0){$checkthis2=round($values[$i]/$total*100);}
else{$checkthis2=0;}
if($checkthis2<$minvalue){$othervalue+=$values[$i];$thingsinother[]=$i;}
}

if($othervalue>0 && count($thingsinother)>1)
{
$options[]="Other";
$values[]=$othervalue;
}


if($stats=='time')
{
$newvalues=$values;
$newoptions=$options;
$quarts=quartiles($newvalues);
$rowheight=18;
$rowspacer=1;

}elseif($stats=='day')
{
$newvalues=$values;
$newoptions=$options;
$quarts=quartiles($newvalues);
if($monthstuff)
{$rowheight=18;$rowspacer=1;}else{$rowheight=25;$rowspacer=3;}

}
else
{
//only sort into order if not times of day
//orders arrays.
$maxvalue=0;
$valueschecked=0;
$tempi=0;
$newvalues=array();
$newoptions=array();
$i=0;
$oldvalues=$values;
//continues until the arrays have been completely reshuffled
while($i<sizeof($values))
{
$maxvalue=0;
for($i2=0;$i2<sizeof($values);$i2++)
{//this searches through and finds the largest number in the values array
if($values[$i2]>$maxvalue){$maxvalue=$values[$i2];$tempi=$i2;}
}
//deletes this number from the values array, but puts it, and it's corrosponding option into the newoptions and newvalues arrays

if(round($values[$tempi]/$total*100)>=$minvalue || $options[$tempi]=='Other' || count($thingsinother)<=1)
{//if the percentage is below the minimum, they've been lumped into 'other' so don't add them (unless this IS other, or there is only one 'other;)
$newvalues[]=$values[$tempi];
$newoptions[]=$options[$tempi];
}
$values[$tempi]=0;

$i++;
}

$rowheight=25;
$rowspacer=3;

}

switch($charttype)
{
case 'pie';
//error_reporting(E_ALL);
require 'piechart.php';

$pie=new piechart();
$pie->settitle($title);
$pie->setsize($chartsize);
$pie->setdata($newoptions,$newvalues);

$extrainfo="Total: ".$total."\n ";

if($othervalue>1)
{
$extrainfo.="\nOther:";
for($i=0;$i<count($thingsinother);$i++)
{
$extrainfo.="\n".$options[$thingsinother[$i]]." (".$oldvalues[$thingsinother[$i]].")";
}

}
$pie->setinfo($extrainfo);
$pie->drawchart();

break;
default;//draw bar chart

//distance from left the bars should start (number of characters in largest name)
$maxlength=0;
for($i=0;$i<sizeof($newoptions);$i++)
{
if(strlen($newoptions[$i])>$maxlength){$maxlength=strlen($newoptions[$i]);}
}


if($total!==0){$maxwidth=$maxlength*9+2+round($newvalues[0]/$total*300)+100;}
else{$maxwidth=$maxlength*9+2+100;}

if(strlen($title)*8>$maxwidth){$maxwidth=strlen($title)*8;}

$maxheight=(sizeof($newoptions)+1)*$rowheight;

$im = imageCreate($maxwidth,$maxheight);
$background = imageColorAllocate($im, 255, 255, 255);
imagecolortransparent($im,$background);
$black = imageColorAllocate ($im, 0, 0, 0); 
$red = imageColorAllocate ($im, 255, 0, 0);
$yellow = imageColorAllocate ($im, 255, 255, 0);
$green = imageColorAllocate ($im, 0, 255, 0);
$blue = imageColorAllocate ($im, 0, 0, 255);




imagestring($im, $fontsize, 0, 0 , $title,  $black);

for($i=0;$i<sizeof($newoptions);$i++)
{
$i2=$i+1;

switch($stats)
{case 'time';
case 'day';
if($newvalues[$i]<$quarts[1]){$colour=$blue;}
if($newvalues[$i]>=$quarts[1] && $newvalues[$i]<$quarts[2]){$colour=$green;}
if($newvalues[$i]>=$quarts[2] && $newvalues[$i]<$quarts[3]){$colour=$yellow;}
if($newvalues[$i]>=$quarts[3]){$colour=$red;}
break;
default;
switch (fmod($i, 4))
{case 0;$colour=$red;break;
case 1;$colour=$yellow;break;
case 2;$colour=$green;break;
default;$colour=$blue;break;}
break;}
//fix division by zero - crude but works.
if($total==0){$total=1;}
//option's string
imagestring($im, $fontsize, 0, $i2*$rowheight+$rowspacer , $newoptions[$i],  $black);
//bar for length of option's value
imagefilledrectangle($im, $maxlength*9+2, $i2*$rowheight+$rowspacer, $maxlength*9+2+round($newvalues[$i]/$total*300), (($i2+1)*$rowheight)-$rowspacer,$colour);
//text for option's value
imagestring($im, $fontsize, $maxlength*9+2+round($newvalues[$i]/$total*300)+5, $i2*$rowheight+$rowspacer , $newvalues[$i].'='.round($newvalues[$i]/$total*100).'%',  $black);
}



header('Content-type: image/png');
imagePNG($im);
imageDestroy($im); 


break;
}
?>
