<?php
//error_reporting(E_ALL);

require 'settings.php';
require 'functions.php';
include 'countersilentphp.php';

$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$starttime=$time;
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<title>View Hit Count Statistics</title>
<style type="text/css">
<!--
body {
/*background-color:#ffffcc;*/
font-family: Helvetica,Arial, "trebuchet MS",  sans-serif;

}

.nomargin{margin:2;}
/*z-indexs: windows:2, menus:3, graph:1, about:5, new lines:4;*/
//-->
</style>
</head>
<body>
<h2>Hit Statistics for <a href="<?php echo $sitelink; ?>"><?php echo $sitename; ?></a></h2>
<form  action="javascript:updatestats()" id="statsform">
Organise by: From-Till<input type="radio" name="orgby" onChange='orgoptions()' checked> or Specific Month<input type="radio" onChange='orgoptions()' name="orgby"><br><br>
<span id="fromtillspan" style='display:block;'>

From: <select name='from' onChange='dateoptions()'><option value='hour'>1 Hour ago</option><option value='day'>1 Day ago</option><option value='week'>1 Week ago</option><option value='month'>1 Month ago</option><option value='ever'>The Beginning</option><option value='date'>Specific Date</option></select>

<span id="extrafromstuff" style='display:none;'>
Hour:<select name='hourfrom'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>
Day:<select name='dayfrom'><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option></select>
Month:<select name='monthfrom'><option value='1'>Jan</option><option value='2'>Feb</option><option value='3'>Mar</option><option value='4'>Apr</option><option value='5'>May</option><option value='6'>Jun</option><option value='7'>Jul</option><option value='8'>Aug</option><option value='9'>Sep</option><option value='10'>Oct</option><option value='11'>Nov</option><option value='12'>Dec</option></select>
Year:<input type='text' name='yearfrom' size=5 value='<?php echo date('Y'); ?>'>
</span>
<br>Untill: <select name='till' onChange='dateoptions()'><option value='now'>Now</option><option value='date'>Specific Date</option></select>
<span id="extratillstuff" style='display:none;'>
Hour:<select name='hourtill'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>
Day:<select name='daytill'><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option></select>
Month:<select name='monthtill'><option value='1'>Jan</option><option value='2'>Feb</option><option value='3'>Mar</option><option value='4'>Apr</option><option value='5'>May</option><option value='6'>Jun</option><option value='7'>Jul</option><option value='8'>Aug</option><option value='9'>Sep</option><option value='10'>Oct</option><option value='11'>Nov</option><option value='12'>Dec</option></select>
Year:<input type='text' name='yeartill' size=5 value='<?php echo date('Y'); ?>'>
</span>

</span>

<span id="specificspan" style="display:none;">
Month:<select name='orgmonthfrom'><option value='1'>Jan</option><option value='2'>Feb</option><option value='3'>Mar</option><option value='4'>Apr</option><option value='5'>May</option><option value='6'>Jun</option><option value='7'>Jul</option><option value='8'>Aug</option><option value='9'>Sep</option><option value='10'>Oct</option><option value='11'>Nov</option><option value='12'>Dec</option></select>
Year:<input type='text' name='orgyearfrom' size=5 value='<?php echo date('Y'); ?>'>
</span>

<br>By: <select name='countby'><option value=2>Unique</option><option value=3>IP</option><option value=1>Hit</option></select>
<input type="submit" value="View">
</form>
<?php



switch($publicstats)
{//if stats aren't set to public, this page will need a password
case false;
require 'secure.php';
break;
}


if($_GET['min']){$minvalue=$_GET['min'];}else{$minvalue=1;}
if($_GET['font']){$fontsize=$_GET['font'];}else{$fontsize=4;}

if($_GET['stats']){$stats=$_GET['stats'];}else{$stats='';}


function checkdatestuff($test,$extra)
{if(is_numeric($test)){return $test;}
else{switch($extra)
{case 'year';return date("Y");break;
default;return 0;break;}}}

$dbtill=time();
$timespan=$_GET['from'];
$monthstuff=false;

switch($_GET['from'])
{case 'hour';
$dbfrom=time()-60*60;
break;
case 'day';
$dbfrom=time()-24*60*60;
break;
case 'month';
//$dbfrom=time()-24*60*60*7*4;
$dbfrom=mktime(date("G"), date("i"), date("s"), date("m")-1, date("d"), date("Y"));
break;
case 'ever';
$dbfrom=0;
break;
case 'date';
$dbfrom=mktime(checkdatestuff($_GET['hourfrom'],false), checkdatestuff($_GET['minutefrom'],false), checkdatestuff($_GET['secondfrom'],false), checkdatestuff($_GET['monthfrom'],false), checkdatestuff($_GET['dayfrom'],false), checkdatestuff($_GET['yearfrom'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
//echo $dbfrom;
//echo '<br>'.date('dS M y H:i',$dbfrom);
//echo "YOOHOO";
break;
case 'specific';//view statistics for a specific month/year/etc.

$monthstuff=true;

$dbfrom=mktime(0, 0, 0, checkdatestuff($_GET['monthfrom'],false),1, checkdatestuff($_GET['yearfrom'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
$dbtill=mktime(0, 0, 0, checkdatestuff($_GET['monthfrom'],false)+1,1, checkdatestuff($_GET['yearfrom'],'year'), -1);
$specifictitle=date('F Y',$dbfrom);

break;
case 'week';
default;
$dbfrom=time()-24*60*60*7;
$timespan='week';
break;}
switch($_GET['till'])
{
case 'date';
$dbtill=mktime(checkdatestuff($_GET['hourtill'],false), checkdatestuff($_GET['minutetill'],false), checkdatestuff($_GET['secondtill'],false), checkdatestuff($_GET['monthtill'],false), checkdatestuff($_GET['daytill'],false), checkdatestuff($_GET['yeartill'],'year'), -1);//last bit set to -1 so PHP tries to do daylight saving time by itself.
break;
}

if($_GET['countby']){$countby=$_GET['countby'];}
else
{//if a count by has been set in the url, use it, otherwise use the default from settings.php
$countby=$statdisplaycount;}




switch($countby)
{case 1;$echocount="Hit";break;
case 2;$echocount="Unique";break;
case 3;$echocount="IP";break;
default;$echocount='Default';$countby=$defaultcountby;break;}

$accountedbrowsers=array('Phoenix','Firebird','Firefox','Minefield','MSIE','MSPIE','Mozilla','SeaMonkey','Bot','Googlebot','Safari','Konqueror','Opera');

$addressstring='from='.$_GET['from'].'&countby='.$countby.'&specific='.$_GET['specific'].'&till='.$_GET['till'].'&hourfrom='.$_GET['hourfrom'].'&dayfrom='.$_GET['dayfrom'].'&monthfrom='.$_GET['monthfrom'].'&yearfrom='.$_GET['yearfrom'].'&hourtill='.$_GET['hourtill'].'&daytill='.$_GET['daytill'].'&monthtill='.$_GET['monthtill'].'&yeartill='.$_GET['yeartill'];

switch($_GET['browser'])
{
case 'Firefox';
$browser=array('Phoenix','Firebird','Firefox','Minefield');
break;
case 'MSIE';
$browser=array('MSIE','MSPIE');
break;
case 'Mozilla';
$browser=array('Mozilla','SeaMonkey');
break;
case 'Bot';
$browser=array('Bot','Googlebot');
break;
case 'Other';
$others=true;
break;
default;
$browser=array($_GET['browser']);
break;
}




$total=0;
$ips=array();
$uniqueips=array();

$versions=array();
$allversions=array();
$versionscount=array();

$OSversions=array();
$allOSversions=array();
$OSversionscount=array();

$times=array('0000','0100','0200','0300','0400','0500','0600','0700','0800','0900','1000','1100','1200','1300','1400','1500','1600','1700','1800','1900','2000','2100','2200','2300');
$timescount=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);



if($monthstuff)
{//Day of Hit will be with dates of the month
$daysinmonth=cal_days_in_month(CAL_GREGORIAN, checkdatestuff($_GET['monthfrom'],false), checkdatestuff($_GET['yearfrom'],'year'));
$days=array();
$dayscount=array();

$month=checkdatestuff($_GET['monthfrom'],false);
$year=checkdatestuff($_GET['yearfrom'],'year');

for($i=1;$i<=$daysinmonth;$i++)
{
//$days[($i-1)]=$i.getend($i,checkdatestuff($_GET['monthfrom']),checkdatestuff($_GET['yearfrom'],'year'));
$days[($i-1)]=date('jS', mktime(1,0,0,$month,$i,$year)).' ('.date('D', mktime(1,0,0,$month,$i,$year)).')';//substr(date('D', mktime(1,0,0,$month,$i,$year)),0,1);
$dayscount[($i-1)]=0;}

}else{
//day of hit will be with day of the week.
$days=array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
$dayscount=array();
}


$pagescount=array();

$browsers=array();
$allbrowsers=array();
$browserscount=array();

$con = mysql_connect($servername, $dbusername, $dbpassword);
if (!$con){die("Could not connect:" . mysql_error());}
mysql_select_db($dbname, $con);

//collect numbers for unique hits, different ips and total hits
$result = mysql_query("SELECT DISTINCT ip FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."';");
$diffips = mysql_num_rows($result); 

$result = mysql_query("SELECT count(hit) FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."' AND `unique`='1';");
$row = mysql_fetch_array($result); 
$uniquehits=$row['count(hit)'];

$result = mysql_query("SELECT count(hit) FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."';");
$row = mysql_fetch_array($result); 
$totalhits=$row['count(hit)'];


switch($countby)
{case 2;//unique hits - will require all data
$result = mysql_query("SELECT `time`,`browser`,`os`,`pages` FROM `$dbtablename` WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."' AND `unique`='1';");
break;
default;//took ages to get working query here - I *think* it selects everything from the table between the two times where each IP is unique
//$result = mysql_query("SELECT $dbtablename.hit,$dbtablename.ip,$dbtablename.time,$dbtablename.browser,$dbtablename.os,$dbtablename.pages FROM $dbtablename RIGHT JOIN ( SELECT DISTINCT ip FROM $dbtablename) AS TR ON TR.ip = $dbtablename.ip WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."';");
//note - scrapped above query in favour of older technique which uses PHP to get different IPs - seems to be much faster.

//this query seems to grind server to a halt:
//$result = mysql_query("SELECT `ip`,`time`,`browser`,`osversion`,`os`,`pages` FROM $dbtablename WHERE ip in (SELECT DISTINCT ip FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."');");


$result = mysql_query("SELECT `ip`,`time`,`browser`,`osversion`,`os`,`pages` FROM $dbtablename WHERE time > '".mysql_real_escape_string($dbfrom)."' AND time < '".mysql_real_escape_string($dbtill)."'");
break;}


while($row = mysql_fetch_array($result))
{
$continue=true;

if($countby==2)
{//stats by UNIQUE IPs
/*
if(!in_array($row['ip'], $uniqueips)){$uniqueips[]=$row['ip'];}else{$continue=false;}

if($row['time']<$dbfrom || $row['time']>$dbtill){$continue=false;}
else{
$totalhits++;
//if it's in the correct time, get data for different IPs
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}
}
*/
//$totalhits++;
}else{
//stats for different IPs or hits
//$totalhits++;
//checks to see if IP is in the ips array, if not adds it.  if it is then decides whether to continue or not.
if(!in_array($row['ip'], $ips)){$ips[]=$row['ip'];}else{switch($countby){case 3;case 2;$continue=false;break;}}
}

if($continue)
{
$total++;
if(!in_array($row['browser'].' '.$row['version'], $versions)){$versions[]=$row['browser'].' '.$row['version'];}
$allversions[]=$row['browser'].' '.$row['version'];

if(!in_array($row['browser'], $browsers)){$browsers[]=$row['browser'];}
$allbrowsers[]=$row['browser'];

if(!in_array($row['os'], $OSversions)){$OSversions[]=$row['os'];}
$allOSversions[]=$row['os'];

$timescount[tohours($row['time'])]++;

if($monthstuff)
{//counts up hits for a specific day of the month.
$dayscount[(date('j',$row['time'])-1)]++;
}else{
//counts up hits for day of week
$dayscount[(date('w',$row['time']))]++;
}

$pagescount[]=$row['pages'];

//end of continue if statement
}

//end of while loop
}

//this works out the number of different versions, and then the number of each version.  I was quite impressed I managed to think it though, but I can't remember how it works anymore
for ($i=0;$i<sizeof($versions);$i++)
{
$versionscount[$i]=0;
for ($i2=0;$i2<sizeof($allversions);$i2++)
{if($allversions[$i2]==$versions[$i]){$versionscount[$i]++;}}
}

for ($i=0;$i<sizeof($browsers);$i++)
{
$browserscount[$i]=0;
for ($i2=0;$i2<sizeof($allbrowsers);$i2++)
{if($allbrowsers[$i2]==$browsers[$i]){$browserscount[$i]++;}}
}

for ($i=0;$i<sizeof($OSversions);$i++)
{
$OSversionscount[$i]=0;
for ($i2=0;$i2<sizeof($allOSversions);$i2++)
{if($allOSversions[$i2]==$OSversions[$i]){$OSversionscount[$i]++;}}
}

//echo "<h2>Statistics for www.lukewallin.co.uk by ".$echocount."</h2>";
if($timespan!=='date' && $timespan!=='specific' && $_GET['till']!=='date')
{
if($timespan!=='ever'){echo "<h3>For the last $timespan by $echocount</h3>";}
else{echo "<h3>All Data by $echocount</h3>";}
}
elseif($_GET['from']=='specific')
{
echo "<h3>$specifictitle by $echocount</h3>";
}else{
//echo "<h3>Since ".date('H:i\, dS M Y',$dbfrom)." until ".date('H:i\, dS M Y',$dbtill)."</h3>";
echo "<h3>";
}

echo "Since ".date('H:i\, dS M Y',$dbfrom)." until ".date('H:i\, dS M Y',$dbtill)." by $echocount<br>";

if($timespan=='date' || $_GET['till']=='date')
{
echo "</h3>";
}



if($countby==2){echo "<b>";}
echo "<br>Unique Hits: $uniquehits";
if($countby==2){echo "</b>";}
if($countby==3){echo "<b>";}
echo "<br>Different IPs: $diffips";
if($countby==3){echo "</b>";}
if($countby==1){echo "<b>";}
echo "<br>Total Hits: $totalhits";
if($countby==1){echo "</b>";}

echo "<br><br>(Now: ".date('H:i\ T, D jS M Y').')<br>';




function spewstats($options,$values,$sort,$extra)
{
//global $title,$total,$minvalue,$timespan,$countby,$dbfrom,$dbtill,$addressstring;
global $addressstring,$minvalue,$total,$dbfrom,$dbtill;

//looks for all the options with less than the minimum percentage of values, and lumps them all into 'other'
$othervalue=0;
for($i=0;$i<sizeof($values);$i++)
{
if($total!==0){$checkthis=round($values[$i]/$total*100);}
else{$checkthis=0;}
if($checkthis<$minvalue){$othervalue+=$values[$i];}
}

if($othervalue>0)
{
$options[]="Other";
$values[]=$othervalue;
}

if($sort)
{//orders arrays.
$maxvalue=0;
$valueschecked=0;
$tempi=0;
$newvalues=array();
$newoptions=array();

//continues until the arrays have been completely reshuffled
for($i=0;$i<sizeof($values);$i++)
{
$maxvalue=0;
for($i2=0;$i2<sizeof($values);$i2++)
{//this searches through and finds the largest number in the values array
if($values[$i2]>$maxvalue){$maxvalue=$values[$i2];$tempi=$i2;}
}
//deletes this number from the values array, but puts it, and it's corrosponding option into the newoptions and newvalues arrays

if(round($values[$tempi]/$total*100)>=$minvalue || $options[$tempi]=='Other')
{//if the percentage is below the minimum, they've been lumped into 'other' so don't add them
$newvalues[]=$values[$tempi];
$newoptions[]=$options[$tempi];
}
$values[$tempi]=0;
}

$rowheight=25;
$rowspacer=3;

//if not sorting
}else{
$newvalues=$values;
$newoptions=$options;

switch($extra)
{
case 'time';
//$avgtime=$total/24;
$realavgtime=$total/(($dbtill-$dbfrom)/3600);


//echo $quarts[1].' '.$quarts[2].' '.$quarts[3];
//echo "total=$total from/3600 = ".($from/3600);
echo "\nAverage hits per hour = ".(round($realavgtime*100)/100)."<p>";
$quarts=quartiles($newvalues);
break;
case 'day';
$realavgday=$total/(($dbtill-$dbfrom)/86400);
echo "\nAverage hits per day = ".(round($realavgday*10)/10)."<p>";
$quarts=quartiles($newvalues);
break;

}

}

//echo '<br>sizeof($newoptions): '.sizeof($newoptions);

echo "\n<table>";
for($i=0;$i<sizeof($newoptions);$i++)
{

switch($extra)
{case 'time';
case 'day';
if($newvalues[$i]<$quarts[1]){$colour='blue';}
if($newvalues[$i]>=$quarts[1] && $newvalues[$i]<$quarts[2]){$colour='green';}
if($newvalues[$i]>=$quarts[2] && $newvalues[$i]<$quarts[3]){$colour='yellow';}
if($newvalues[$i]>=$quarts[3]){$colour='red';}
break;
default;
switch (fmod($i, 4))
{case 0;$colour='red';break;
case 1;$colour='yellow';break;
case 2;$colour='green';break;
default;$colour='blue';break;}
break;}


$count=($i+1).".";
if(!$sort)
{$count='';}
if($total!==0){$sizestuff=round($newvalues[$i]/$total*500);$percent=round($sizestuff/5);}
else{$sizestuff=0;$percent=0;}

switch($extra)
{
case 'browser';
switch($newoptions[$i])
{case 'Other';
$browserlink="<a href='browserstats.php?browser=".$newoptions[$i]."&pc=$percent&others=".serialize($newoptions)."&$addressstring'>".$newoptions[$i].":</a> ";
break;
default;
$browserlink="<a href='browserstats.php?browser=".$newoptions[$i]."&pc=$percent&$addressstring'>".$newoptions[$i].":</a> ";
break;}



break;
case 'platform';

if($newoptions[$i]=='Other')
{
$browserlink="<a href='OSstats.php?os=Other&others=".serialize($newoptions)."&pc=$percent&$addressstring'>".$newoptions[$i].":</a> ";
}else{
$browserlink="<a href='OSstats.php?os=".$newoptions[$i]."&pc=$percent&$addressstring'>".$newoptions[$i].":</a> ";
}
break;
default;
$browserlink=$newoptions[$i].": ";
break;
}




echo "\n<tr><td>".$count." $browserlink</td><td><div style='height:20;width:".$sizestuff.";background-color:$colour;float:left'></div></td><td> ".$newvalues[$i].' = '.$percent.'%</td></tr>';
}

echo "\n</table>";
//end of spewstats function.
}



echo"\n<br><h3 class='nomargin'>Browsers</h3><a href=\"statsimage.php?stats=browser&min='0'&$addressstring&charttype=pie\">Pie Chart</a> <a href=\"statsimage.php?stats=browser&min='0'&$addressstring\">Bar Chart</a><p>";
spewstats($browsers,$browserscount,true,'browser');

//$title="Versions of ".$_GET['browser']." by ".$echocount;
//spewstats($versions,$versionscount,$title);
echo "\n<h3 class='nomargin'>Platform</h3><a href=\"statsimage.php?stats=os&countby=$countby&min='0'&$addressstring&charttype=pie\">Pie Chart</a> <a href=\"statsimage.php?stats=os&countby=$countby&min='0'&$addressstring\">Bar Chart</a><p>";
spewstats($OSversions,$OSversionscount,true,'platform');

if($howtocount!==0 && $showpagesviewed)
{//if the counter doesn't count every time it's viewed, show stats about number of pages viewed per visit.
echo "\n<h3><a href='cumfreq.php?values=".serialize($pagescount)."'>Pages Viewed</a></h3>";
//echo "<u>Cumulative Frequency</u><br>";
//echo "\n<img src='cumfreq.php?values=".serialize($pagescount)."' alt=\"cumulative frequency\">";
sort($pagescount);
$totalfreq=count($pagescount);
$min=$pagescount[0];
$max=$pagescount[$totalfreq-1];
$median=round($pagescount[($totalfreq+1)/2]);
$q1=round($pagescount[($totalfreq+1)/4]);
$q3=round($pagescount[($totalfreq+1)/4*3]);
$totalcumfreq=0;

for($i=0;$i<count($pagescount);$i++){$totalcumfreq+=$pagescount[$i];}
$mean=round($totalcumfreq/$totalfreq*10)/10;

echo "Min pages viewed: $min<br>";
echo "Median pages viewed: $median<br>";
echo "Mean pages viewed: $mean<br>";
echo "Max pages viewed: $max<br>";
//echo 'count($pagescount): '.$totalfreq;

}

echo "\n<h3><a href=\"statsimage.php?stats=day&countby=$countby&min='0'&$addressstring\">Day of Visit</a></h3>";
spewstats($days,$dayscount,false,'day');

echo "\n<h3><a href=\"statsimage.php?stats=time&countby=$countby&min='0'&$addressstring\">Time of Visit (24 Hours GMT)</a></h3>";
spewstats($times,$timescount,false,'time');

echo "<br>";


$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$endtime=$time;
$totaltime=$endtime-$starttime;
$totaltime=round($totaltime*100)/100;

echo "PHP generated this page in $totaltime seconds.<br><a href='http://www.lukewallin.co.uk/index.php?go=counter'>Script Copyright Luke Wallin 2007, released under GPL</a>";
?>

<script language="JavaScript" type="text/javascript">
<!--

var statvar=document.getElementById('statsform')

function updatestats()
{
if(statvar.orgby[0].checked){//from->till
location.href='viewstats.php?countby='+statvar.countby.value+'&from='+statvar.from.value+'&hourfrom='+statvar.hourfrom.value+"&dayfrom="+statvar.dayfrom.value+"&monthfrom="+statvar.monthfrom.value+"&yearfrom="+statvar.yearfrom.value+"&till="+statvar.till.value+"&hourtill="+statvar.hourtill.value+"&daytill="+statvar.daytill.value+"&monthtill="+statvar.monthtill.value+"&yeartill="+statvar.yeartill.value
}else{
location.href='viewstats.php?countby='+statvar.countby.value+"&from=specific&specific=month&monthfrom="+statvar.orgmonthfrom.value+"&yearfrom="+statvar.orgyearfrom.value
}
}

//var loopme=setInterval('dateoptions()',200);

var fromdateoptionsopen=false;
var tilldateoptionsopen=false;

function dateoptions()
{
if(!fromdateoptionsopen && document.getElementById('statsform').from.value=='date')
{//if 'specific date' is selected, but the extra options aren't displayed
fromdateoptionsopen=true;
document.getElementById('extrafromstuff').style.display='block';}
else if(fromdateoptionsopen && document.getElementById('statsform').from.value!=='date')
{//the extra options ARE open, but not wanted beause 'specific date' isn't selected.
fromdateoptionsopen=false;
document.getElementById('extrafromstuff').style.display='none';}

if(!tilldateoptionsopen && document.getElementById('statsform').till.value=='date')
{tilldateoptionsopen=true;
document.getElementById('extratillstuff').style.display='block';}
else if(tilldateoptionsopen && document.getElementById('statsform').till.value!=='date')
{tilldateoptionsopen=false;
document.getElementById('extratillstuff').style.display='none';}
}


function orgoptions()
{//how to organise stats, using from->till or a specific month
if(statvar.orgby[0].checked)
{//from->till
document.getElementById('fromtillspan').style.display='block';
document.getElementById('specificspan').style.display='none';
}
else
{//specific month
document.getElementById('fromtillspan').style.display='none';
document.getElementById('specificspan').style.display='block';
}


}

if(document.all)
{//for some reason IE doesn't like the onchange stuff, so it has to use a loop.
var loopme=setInterval('loop()',200)}

function loop()
{
orgoptions()
dateoptions()
}

orgoptions()
dateoptions()

//alert('<?php echo $_GET['from']; ?>'=='specific')

//this bit sets the form options to what is currently being displayed on the page.

if('<?php echo $timespan; ?>'=='specific')
{//certain month
statvar.orgby[0].checked=false;
statvar.orgby[1].checked=true;
document.getElementById('statsform').orgmonthfrom.value='<?php echo $_GET['monthfrom']; ?>';
document.getElementById('statsform').orgyearfrom.value='<?php echo $_GET['yearfrom']; ?>';
orgoptions()
}else if('<?php echo $timespan; ?>'=='date')
{//a from date
statvar.orgby[1].checked=false;
statvar.orgby[0].checked=true;
orgoptions();
document.getElementById('statsform').from.value='date';
dateoptions();
document.getElementById('statsform').hourfrom.value='<?php echo $_GET['hourfrom']; ?>';
document.getElementById('statsform').dayfrom.value='<?php echo $_GET['dayfrom']; ?>';
document.getElementById('statsform').monthfrom.value='<?php echo $_GET['monthfrom']; ?>';
document.getElementById('statsform').yearfrom.value='<?php echo $_GET['yearfrom']; ?>';
}else{//the old for last week/day/hour
statvar.orgby[1].checked=false;
statvar.orgby[0].checked=true;
orgoptions();
document.getElementById('statsform').from.value='<?php echo $timespan;?>';}


if('<?php echo $_GET['till']; ?>'=='date')
{
document.getElementById('statsform').till.value='date';
dateoptions();
document.getElementById('statsform').hourtill.value='<?php echo $_GET['hourtill']; ?>';
document.getElementById('statsform').daytill.value='<?php echo $_GET['daytill']; ?>';
document.getElementById('statsform').monthtill.value='<?php echo $_GET['monthtill']; ?>';
document.getElementById('statsform').yeartill.value='<?php echo $_GET['yeartill']; ?>';
}else
{
document.getElementById('statsform').till.value='now';
dateoptions();
}

document.getElementById('statsform').countby.value='<?php echo $countby;?>'


//PHPtoelement('viewstatsphp.php', 'statsspan')
//updatestats() 
//-->
</script>
</body>
</html>

