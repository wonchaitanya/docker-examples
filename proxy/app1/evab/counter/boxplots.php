<?php

$title=$_GET['title'];
$values=unserialize($_GET['values']);
sort($values);
$totalvalues=count($values);

$min=$values[0];
$max=$values[$totalvalues-1];
$fontsize=4;

$totalcumfreq=0;
for($i=0;$i<count($values);$i++)
{
$totalcumfreq+=$values[$i];
}

$mean=$totalcumfreq/$totalvalues;

$median=round($values[($totalvalues+1)/2]);

$q1=round($values[($totalvalues+1)/4]);
$q3=round($values[($totalvalues+1)/4*3]);

$width=500;
$height=300;
$margin=30;

$im = imageCreate($width+$margin,$height+$margin);
$background = imageColorAllocate($im, 255, 255, 255);
imagecolortransparent($im,$background);
$black = imageColorAllocate ($im, 0, 0, 0); 
$red = imageColorAllocate ($im, 255, 0, 0);
$yellow = imageColorAllocate ($im, 255, 255, 0);
$green = imageColorAllocate ($im, 0, 255, 0);
$blue = imageColorAllocate ($im, 0, 0, 255);

imagestring($im, $fontsize, 0, 0 , "Min = $min",  $black);
imagestring($im, $fontsize, 0, 20 , "Q1 = $q1",  $black);
imagestring($im, $fontsize, 0, 40 , "Median = $median",  $black);
imagestring($im, $fontsize, 0, 60 , "Q3 = $q3",  $black);
imagestring($im, $fontsize, 0, 80 , "Max = $max",  $black);
imagestring($im, $fontsize, 0, 100 , "Mean = ".round($mean),  $black);

header('Content-type: image/png');
imagePNG($im);
imageDestroy($im); 

?>