# Load Balanced App
This example uses an NGINX container to load balance 3 app instances.

## build, tear down
+ `$ docker-compose up`
+ `$ docker-compose down`
